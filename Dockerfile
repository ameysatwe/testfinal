# This state compile our TypeScript to get the JavaScript code

FROM node:12.13.0 AS builder
WORKDIR /usr/src/app
COPY package*.json ./
COPY tsconfig*.json ./
COPY . .
RUN npm ci --quiet && npm run build
#WORKDIR ./src/app/build
RUN ls -ltr
RUN echo "i AM here"

#nginx serving
#FROM nginx:1.14-alpine
#COPY --from=builder /usr/src/app/build /usr/share/nginx/html/
#COPY --from=builder /usr/src/app/src/*.html /usr/share/nginx/html


# Generation of assets
FROM httpd:alpine
RUN apk add --no-cache git && apk update && apk add wget && apk add zip unzip
#COPY ./ /usr/local/apache2
COPY --from=builder /usr/src/app/simulation /usr/local/apache2/htdocs/build
WORKDIR ./usr/local/apache2/htdocs/build
RUN ls -ltr
#RUN chmod +x /usr/local/apache2/script-new.sh 
#RUN /usr/local/apache2/script-new.sh
# /usr/src/app/Simulation/css  
# /usr/src/app/Simulation/*.html
