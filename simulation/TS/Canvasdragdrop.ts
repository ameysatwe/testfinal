//declarations
// var canvas1:HTMLcanvas1Element = <HTMLcanvas1Element> document.getElementById("mycanvas1");
// var context:canvas1RenderingContext2D = <canvas1RenderingContext2D> canvas1.getContext("2d");

var component: String;
var action: String;
var enabledrag: boolean = false;
var position_radius: number;
canvas1.addEventListener("click", mouseclick, false);
canvas1.addEventListener("mouseup", mouseup, false);
canvas1.addEventListener("mousedown", mousedown, false);
canvas1.addEventListener("mousemove", mousemove, false);
var rect = canvas1.getBoundingClientRect();

var intialPoint: CondensingSleeve.Point;
var sleeve1: CondensingSleeve.CondensingSleeveDiagram;

let sleeveDrawn = false; // added so that components do not get redrawn
let tankDrawn = false;
let pumpDrawn = false;
let rotameterDrawn = false;
let sleeveInPlace = false; //these will help in redrawing components in original_structure once they get fixed
let tankInPlace = false;
let rotameterInPlace = false;
let pumpInPlace = false;
function original_structure() {
    ///************************PIPELINE**************** */
    drawPIPE();
    // //************POWER BUTTONS P AND H*************** */
    var buttonContainer: Geometries.Rectangle = new Geometries.Rectangle(new Geometries.Point(600, 120), 40, 90, "white");
    buttonContainer.draw(context);

    var button: Buttons.Button = new Buttons.Button(new Geometries.Circle(15, new Geometries.Point(625, 140)), context, "red");
    button.draw();
    button.setText("P");

    var button1: Buttons.Button = new Buttons.Button(new Geometries.Circle(15, new Geometries.Point(665, 140)), context, "red");
    button1.draw();
    button1.setText("H");
    if (sleeveInPlace) {
        sleeve1.draw();
    }
    if (tankInPlace) {
        a.drawTank();
        a.drawHeater();
    }
    if (pumpInPlace) {
        pump.draw();
    }
    if(rotameterInPlace) {
        container.draw(context);
    }
}
// var a1:Hwtank.tank;


function drawHWTANK() {
    console.log("HWTANk");
    component = "HWTANK";
}
function drawCondensingSleeve() {
    intialPoint = new CondensingSleeve.Point(200, 200);
    sleeve1 = new CondensingSleeve.CondensingSleeveDiagram(intialPoint, context, 170, 8);
    // console.log("CondensingSleeve");
    component = "CondensingSleeve";
    console.log(component);
}

function drawRotameter() {
    canvas.addEventListener("click", mouseclick, false);
    component = "rotameter";
}
function drawPump() {
    // canvas.addEventListener("click",mouseclick,false);
    component = "pump";
}

function mouseclick(e: MouseEvent) {
    console.log("mouseclick", component)
    var x = e.clientX - rect.left;
    var y = e.clientY - rect.top;
    console.log(x, y);
    if (true) { //added check here for drag
        if (component == "HWTANK" && !tankDrawn) {
            // call draw function for that part
            console.log("HWTANK");
            a = new Hwtank.tank(context, x, y, 125, 180, 10, "black");
            original_structure();
            a.drawTank();
            a.drawHeater();
            // canvas1.removeEventListener("click",mouseclick,false);
            tankDrawn = true; //so that it does not redraw
        }

        else if (component == "CondensingSleeve" && !sleeveDrawn) {
            console.log("CondensingSleeveMouseclick");
            context.clearRect(0, 0, canvas1.width, canvas1.height); //remove already drawn components
            // changing the start of sleeve so that there remains only one sleeve on canvas
            original_structure();
            sleeve1.start.x = x; //change start of sleeve
            sleeve1.start.y = y;
            sleeve1.highlightRed(" ");
            sleeve1.draw(); //draw sleeve
            sleeveDrawn = true;
        }
        else if (component == "pump" && !pumpDrawn) {
            let p1: Geometries.Point = new Geometries.Point(x, y);
            let actualpt: Geometries.Point = new Geometries.Point(220, 490);
            if (actualpt.getLength(p1) < 50) {
                pump = new Pump.PumpContainer(actualpt, 30, context);
                pumpInPlace = true;
            } else {
                pump = new Pump.PumpContainer(p1, 30, context);
            }
            pump.draw();
            pumpDrawn = true;
        }
        else if (component == "rotameter" && !rotameterDrawn) {
            let p1: Geometries.Point = new Geometries.Point(x, y);
            let actualpt: Geometries.Point = new Geometries.Point(184, 140);
            if (actualpt.getLength(p1) < 50) {
                container = new Rotameter.RotameterContainer(actualpt, 110, 40, context);
            }
            else {
                container = new Rotameter.RotameterContainer(p1, 110, 40, context);
            }
            container.draw(context);
            rotameterDrawn = true;
        }
    }
}

function drag() {
    console.log("drag");
    action = "drag";
}

function mousemove(e: MouseEvent) {
    console.log("mousemove");
    var x = e.clientX - rect.left;
    var y = e.clientY - rect.top;
    let point = new CondensingSleeve.Point(x, y);
    if (enabledrag) {
        if (component == "HWTANK" && !tankInPlace) {
            console.log("Drawing Tank")
            context.clearRect(0, 0, canvas1.width, canvas1.height);
            original_structure();
            a = new Hwtank.tank(context, x, y, 125, 180, 10, "black");
            a.drawTank();
            a.drawHeater();
            position_radius = Math.sqrt(Math.pow(415 - x, 2) - Math.pow(503 - y, 2))
            if (position_radius < 15) {
                context.clearRect(0, 0, canvas1.width, canvas1.height);
                original_structure();
                a = new Hwtank.tank(context, 415, 523, 125, 180, 10, "black");
                a.drawTank();
                a.drawHeater();
                enabledrag = false;
                tankInPlace = true;
            }
        }
        else if (component == "CondensingSleeve" && !sleeveInPlace) {
            console.log("dragCondensingSleeve");
            point.y -= 100; //fix for right click
            sleeve1.start = point;
            context.clearRect(0, 0, canvas1.width, canvas1.height);
            original_structure();
            sleeve1.draw();
            // let x = e.x - rect.left;
            // let y = e.y - rect.top;
            if (Math.abs(x - 500) < 70 || Math.abs(y - 125) < 70) {
                sleeve1.start = new CondensingSleeve.Point(500, 125);
                context.clearRect(0, 0, canvas1.width, canvas1.height);
                original_structure();
                sleeve1.draw();
                enabledrag = false; //stop drag
                sleeveInPlace = true; //it will redraw components in original_structure
            }
        } else if (component === 'pump' && !pumpInPlace) {
            let p1: Geometries.Point = new Geometries.Point(x, y);
            if (pump.isInside(p1)) {
                context.clearRect(0, 0, canvas.width, canvas.height);
                original_structure();
                let actualpt: Geometries.Point = new Geometries.Point(220, 490);
                if (actualpt.getLength(p1) < 50) {
                    pump = new Pump.PumpContainer(actualpt, 30, context);
                    pumpInPlace = true;
                }
                else {
                    pump = new Pump.PumpContainer(p1, 30, context);
                }
                pump.draw();
            }
        } else if(component === 'rotameter' && !rotameterInPlace){
            let p1 : Geometries.Point = new Geometries.Point(x,y);
            if(container.isInside(p1)) {
                context.clearRect(0,0,canvas.width,canvas.height);
                let actualpt : Geometries.Point = new Geometries.Point(184,160);
                if(actualpt.getLength(p1) < 70) {
                    container = new Rotameter.RotameterContainer(actualpt,110,40,context);
                    rotameterInPlace = true
                }
                else {
                    container = new Rotameter.RotameterContainer(new Geometries.Point(p1.x - container._width/2,p1.y - container._height/2),110,40,context);
                }
                original_structure();
                container.draw(context);
            }
        }

    }

}



// else if(enabledrag && sleeve1.isInside(point))
// {
//     point.y -=100; //fix for right click
//     sleeve1.start = point;
//     context.clearRect(0,0,canvas1.width, canvas1.height);
//     original_structure();
//     sleeve1.draw();
// }


function mousedown(e: MouseEvent) {
    if (action == "drag") {
        enabledrag = true;
    }
}

function mouseup(e: MouseEvent) {
    console.log("mouseup");
    // var x =e.clientX - rect.left;
    // var y = e.clientY - rect.top; 
    if (action == "drag") {
        enabledrag = false;

        // if(Math.abs(x-500)<70 || Math.abs(y-125)<70){
        //     sleeve1.start = new CondensingSleeve.Point(500,125);
        //     context.clearRect(0,0, canvas1.width, canvas1.height);
        //     original_structure();
        //     sleeve1.draw();
        // }
    }

}

