let temperature = <HTMLSelectElement> document.getElementById("temperature");
let flowrate: number = 0;  // Flowrate

let Ti: number[];  // Ti values
let ti: number[];  // T0 values
let T0: number[];  // ti values
let t0: number[];  // t0 values

// Set values of Ti, T0, ti & t0
function setValues() {

    Ti = [72, 72, 72, 72, 72, 72];
    ti = [31.3, 31.2, 31.2, 31.2, 31.1, 31.0];
    T0 = [67.2, 66.3, 65.1, 63.8, 62.4, 60.0];
    t0 = [36.6, 36.5, 36.3, 36.1, 35.8, 35.0];

}

// Create the Observation table on the page
function createObservationTable() {

    let div = document.getElementById("d4");

    // Create table element
    let table = <HTMLTableElement> document.createElement("table");
    table.id = "observations";

    let row = <HTMLTableRowElement> table.insertRow();
    
    var cell = <HTMLTableCellElement> row.insertCell();
    cell.textContent = "Obs No.";

    var cell = <HTMLTableCellElement> row.insertCell();
    cell.textContent = "Flow Rate Q(L/h)";
    
    var cell = <HTMLTableCellElement> row.insertCell();
    cell.innerHTML = "Ti(&deg; C)";

    var cell = <HTMLTableCellElement> row.insertCell();
    cell.innerHTML = "T0(&deg; C)";

    var cell = <HTMLTableCellElement> row.insertCell();
    cell.innerHTML = "ti(&deg; C)";

    var cell = <HTMLTableCellElement> row.insertCell();
    cell.innerHTML = "t0(&deg; C)";

    table.style.cssFloat = "right";

    table.border = "1";
    // table.style.border = "solid black";
    table.style.borderCollapse = "collpase";
    
    // Add the table to div #d4 after the canvas element
    div.insertBefore(table, div.childNodes[1]);

}

// Function to add observations dynamically on the table
function addObservations() {
    
    if(flowrate == 0) {
        flowrate = 660;
    }
    else {
        if(flowrate > 260) {
            flowrate -= 100;
        }
        else {
            flowrate -= 80;
        }
    }

    let table = <HTMLTableElement>document.getElementById("observations");
    let num = Math.round((660 - flowrate) / 100);
    console.log("Num: " + num);

    if(num > 0) {
        let goAhead: boolean = true;
        // setValues();

        let Ti_ = <HTMLInputElement> document.getElementById("Ti_" + (num - 1).toString());
        let T0_ = <HTMLInputElement> document.getElementById("T0_" + (num - 1).toString());
        let ti_ = <HTMLInputElement> document.getElementById("ti_" + (num - 1).toString());
        let t0_ = <HTMLInputElement> document.getElementById("t0_" + (num - 1).toString());

        Ti_.style.border = "inset";
        T0_.style.border = "inset";
        ti_.style.border = "inset";
        t0_.style.border = "inset";

        if(Ti_.value == "" || +Ti_.value != Ti[num - 1]) {
            Ti_.style.border = "inset red";
            goAhead = false;
        }
        if(T0_.value == "" || +T0_.value != T0[num - 1]) {
            T0_.style.border = "inset red";
            goAhead = false;
        }
        if(ti_.value == "" || +ti_.value != ti[num - 1]) {
            ti_.style.border = "inset red";
            goAhead = false;
        }
        if(t0_.value == "" || +t0_.value != t0[num - 1]) {
            t0_.style.border = "inset red";
            goAhead = false;
        }

        if(!goAhead) { 
            flowrate += 100;
            return;
        }
    }

    if(flowrate < 180) {
        flowrate += 80;
        alert("Reached minimum limit for flowrate!");
        console.log("Flowrate Inside: " + flowrate);
        return;
    }
    
    console.log("Flowrate: " + flowrate);
    var row = table.insertRow();
    row.style.textAlign = "center";

    var cell = row.insertCell();
    cell.textContent = (num + 1).toString();

    var cell = row.insertCell();
    cell.textContent = flowrate.toString();

    // var textbox = <HTMLInputElement>document.createElement("input");
    // textbox.type = "text";
    // textbox.id = "Ti_" + num.toString();
    // textbox.style.width = "40px";
    // cell.appendChild(textbox);

    var cell = row.insertCell();
    var textbox = <HTMLInputElement>document.createElement("input");
    textbox.type = "text";
    textbox.id = "Ti_" + num.toString();
    textbox.style.width = "40px";
    cell.appendChild(textbox);

    var cell = row.insertCell();
    var textbox = <HTMLInputElement>document.createElement("input");
    textbox.type = "text";
    textbox.id = "T0_" + num.toString();
    textbox.style.width = "40px";
    cell.appendChild(textbox);

    var cell = row.insertCell();
    var textbox = <HTMLInputElement>document.createElement("input");
    textbox.type = "text";
    textbox.id = "ti_" + num.toString();
    textbox.style.width = "40px";
    cell.appendChild(textbox);

    var cell = row.insertCell();
    var textbox = <HTMLInputElement>document.createElement("input");
    textbox.type = "text";
    textbox.id = "t0_" + num.toString();
    textbox.style.width = "40px";
    cell.appendChild(textbox);

    showAllValues();

}

// Show all values on the respective container elements
function showAllValues() {
    
    setValues();
    hotFluidInlet.showText(Ti[Math.round((660 - flowrate) / 100)].toString());
    hotFluidOutlet.showText(ti[Math.round((660 - flowrate) / 100)].toString());
    condensingSleeve.showLeftText(t0[Math.round((660 - flowrate) / 100)].toString());
    condensingSleeve.showRightText(T0[Math.round((660 - flowrate) / 100)].toString());
    showFlowRate();
}

// Show the flowrate above the < >  buttons
function showFlowRate() {
    
    clearFlowRate();

    let stpt = btn1.origin_point;
    context.fillStyle = "black";
    context.font = "15px Arial";
    context.fillText("Flowrate: " + flowrate.toString(), stpt.x, stpt.y - 10);

}

// Clear the written flowrate
function clearFlowRate() {
    let stpt = btn1.origin_point;
    context.clearRect(stpt.x - 10, stpt.y - 31, 100, 30);
}

function showValues() {
    
    if(temperature.value == "") {
        alert("Select the temprature");
        return;
    }

    flowrate = 0;
    createObservationTable();
    addObservations();

}