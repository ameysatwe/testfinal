namespace HeatTransferLab {
    export class Point {
        private _x: number;
        private _y: number;
        
        constructor(x: number, y: number) {
            this._x = x;
            this._y = y;
        }

        get x() {
            return this._x;
        }

        get y() {
            return this._y;
        }
    }

    //Core-class Lab 
    export class Lab {
        public canvas: HTMLCanvasElement;
        public context: CanvasRenderingContext2D;
        public originPoint: Point;  // Start / Origin point of the lab
        public path: Path2D[] = [];        // Path to draw on
        public color: string = "black";

        constructor(canvas: HTMLCanvasElement, context: CanvasRenderingContext2D, originPoint: Point) {
            this.canvas = canvas;
            this.context = context;
            this.originPoint = originPoint;
        }

        draw() {

        }

        //Check is a point is inside a path or not
        isInside(pt: Point) {
            let inside = false;
            for(let i = 0; i < this.path.length && !inside; i++) {
                inside = this.context.isPointInPath(this.path[i], pt.x, pt.y);
            }
            return inside;
        }

    }

    //Pipleine Class inherits Lab class
    export class Pipeline extends Lab {
        private widths: number[];   // Widths of 8 parts of pipeline
        private heights: number[];  // Corresponding Heightsd of pipeline
        private rotameterHeight: number; // Height of the rotameter
        public points: Point[] = [];
        private l: number[] = [];
        public inAnimation: boolean[] = [];
        public valveColor: string = "black";

        constructor(originPoint: Point, widths: number[], heights: number[], rotameterHeight: number, context: CanvasRenderingContext2D, canvas: HTMLCanvasElement) {
            super(canvas, context, originPoint); // Super class constructor
            this.widths = widths;
            this.heights = heights;
            this.rotameterHeight = rotameterHeight;
            this.l.push(0, 0, 0, 0, 0, 0, 0, 0);
            this.inAnimation.push(false, false, false, false, false, false, false, false);
        }

        //Draw method overrided
        draw() {
            // this.path = new Path2D(); // Creating a new Path2D Object
            let points: Point[] = []; // List of origin points for each part of pipeline

            //Drawing rectangles on the path to create a pipeline
            
            //First Part
            this.path.push(new Path2D());
            points.push(this.originPoint);
            this.path[0].rect(points[0].x, points[0].y, this.widths[0], -this.heights[0]);
            this.points.push(points[0]);

            //Second Part
            this.path.push(new Path2D());
            points.push(new Point(this.originPoint.x, this.originPoint.y - (this.heights[0] + 10)));
            this.path[1].rect(points[1].x, points[1].y, this.widths[1], -this.heights[1]);
            this.points.push(points[1]);
          
            //Third Part
            this.path.push(new Path2D());
            points.push(new Point(this.originPoint.x, points[1].y - this.rotameterHeight));
            this.path[2].rect(points[2].x, points[2].y, this.widths[2], -this.heights[2]);
            this.points.push(points[2]);
            
            //Extra line

            this.path[0].moveTo(this.originPoint.x, this.originPoint.y);
            this.path[0].lineTo(this.originPoint.x, this.originPoint.y + 5);

            //Fourth Part
            this.path.push(new Path2D());
            points.push(new Point(this.originPoint.x + this.widths[2], points[2].y - this.heights[2]));
            this.path[3].rect(points[3].x, points[3].y, this.widths[3], this.heights[3]);
            this.points.push(points[3]);
            
            //Fifth Part
            this.path.push(new Path2D());
            points.push(new Point(this.originPoint.x + this.widths[3], points[3].y + this.heights[3]));
            this.path[4].rect(points[4].x, points[4].y, this.widths[4], this.heights[4]);
            this.points.push(points[4]);
            
            //Sixth Part
            this.path.push(new Path2D());
            points.push(new Point(points[4].x + this.widths[4], points[4].y + points[3].y));
            this.path[5].rect(points[5].x, points[5].y, this.widths[5], this.heights[5]);
            this.points.push(points[5]);
            
            //Seventh Part
            this.path.push(new Path2D());
            points.push(new Point(points[5].x, (this.heights[4] * 0.83) + points[3].y));
            this.path[6].rect(points[6].x, points[6].y, this.widths[6], this.heights[6]);
            this.points.push(points[6]);
            
            //Eighth Part
            this.path.push(new Path2D());
            points.push(new Point(this.originPoint.x + 30, this.originPoint.y + this.heights[7]+16));
            this.path[7].rect(points[7].x, points[7].y, this.widths[7], this.heights[7]);
            this.points.push(points[7]);
            
            //Arc Closure
            this.path[7].arc(points[7].x, this.originPoint.y + (this.heights[7] * 1.5)+16, this.heights[7] / 1.7, 0.5 * Math.PI, 1.5 * Math.PI);
            
            
            //Drawing the pipline path on canvas
            for(let i = 0; i < this.path.length; i++) {
                this.context.beginPath();
                this.context.strokeStyle = this.color;
                this.context.lineWidth = 2;            
                this.context.stroke(this.path[i]);
            }
            
            this.drawValve();
            
            //Clearing the intersections of rectangles

            //CLear 1st intersection
            this.context.clearRect(points[0].x + 1.5, points[0].y + 2, this.widths[0] - 2, -this.heights[0]);
            
            //clear 2nd intersection
            this.context.clearRect(points[3].x - 1.5, points[3].y + 1, this.heights[2], this.heights[3]-2);

            //clear 3rd intersection
            this.context.clearRect(points[4].x + 1.5, points[4].y + 1.5, this.heights[5] - 2, -this.widths[4] + 2);

            //clear 4th intersection
            this.context.clearRect(points[5].x - 1.5, points[5].y + 1, this.widths[5] + 4, this.heights[5] - 2);

            //clear 5th intersection
            this.context.clearRect(points[6].x - 1.5, points[6].y + 1, this.widths[6] + 4, this.heights[6] - 2);

            //clear 6th intersection
            this.context.clearRect(points[4].x + 1.5, points[4].y + this.heights[4] - 1.5, this.widths[4] - 2, 2);

            //clear 7th intersection
            this.context.clearRect(points[7].x - 1.5, points[7].y + 1, this.widths[7] + 2.5, this.heights[7] - 2);

            // this.context.clearRect(points[2].x + 1, points[2].y - 399, 300, this.widths[0] - 1.5);
            // this.context.clearRect(this.originPoint.x + 1, this.originPoint.y - 1, this.pWidth - 2, 3);
            // this.context.clearRect(this.originPoint.x + 301, this.originPoint.y - 399, this.pWidth - 2, 300);
            // this.context.clearRect(this.originPoint.x + 301, this.originPoint.y - 349 + this.pWidth, 30 + this.pWidth, this.pWidth - 2);
            // this.context.clearRect(this.originPoint.x + 301, this.originPoint.y - 199 + this.pWidth, 30 + this.pWidth, this.pWidth - 2);
            // this.context.clearRect(this.originPoint.x + 29, this.originPoint.y + this.pWidth + 1, 103, this.pWidth - 2);

        }

        // Draw valve with a color
        drawValve(color: string = "black") {
            //Valve
            // console.log(this.originPoint.x, this.originPoint.y - this.heights[0]);
            this.path.push(new Path2D());
            this.path[8].moveTo(this.originPoint.x, this.originPoint.y - this.heights[0]);
            this.path[8].lineTo(this.originPoint.x + this.widths[1], this.originPoint.y - (this.heights[0] + 10));
            this.path[8].moveTo(this.originPoint.x + this.widths[0], this.originPoint.y - this.heights[0]);
            this.path[8].lineTo(this.originPoint.x, this.originPoint.y - (this.heights[0] + 10));

            this.context.strokeStyle = color;
            this.context.stroke(this.path[8]);

            this.valveColor = color;
        }

        fillPipe5() {
            let fillPath = new Path2D();
            let stpt = new Point(this.points[4].x, this.points[4].y - 1);
            // console.log("here");
            if(stpt.y + this.l[4] + 1 >= this.points[4].y + this.heights[4]) {
                this.inAnimation[4] = false;
            }
            else {
                this.l[4] += 5;
            }
            fillPath.rect(stpt.x + 1, stpt.y, this.widths[4] - 2, this.l[4]);
            this.context.fillStyle = "skyblue";
            this.context.fill(fillPath);
        }

        fillPipe4() {
            let fillPath = new Path2D();
            let stpt = new Point(this.points[3].x - 1, this.points[3].y);
            if(stpt.x + this.l[3] + 1 >= this.points[3].x + this.widths[3]) {
                this.inAnimation[3] = false;
            }
            else {
                this.l[3] += 5;
            }
            fillPath.rect(stpt.x, stpt.y + 1, this.l[3], this.heights[3] - 2);
            this.context.fillStyle = "skyblue";
            this.context.fill(fillPath);
        }

        fillPipe3() {

            let fillPath = new Path2D();
            let stpt = new Point(this.points[2].x, this.points[2].y + 1);
            // console.log("here");
            if(stpt.y - this.l[2] - 1 <= this.points[2].y - this.heights[2]) {
                this.inAnimation[2] = false;
            }
            else {
                this.l[2] += 3;
            }
            fillPath.rect(stpt.x + 1, stpt.y, this.widths[2] - 2, -this.l[2]);
            this.context.fillStyle = "skyblue";
            this.context.fill(fillPath);
        }

        // Fill pipe 2 - Animation
        fillPipe2() {
            this.path.push(new Path2D());
            let stpt = new Point(this.points[1].x, this.points[1].y + 1);
            // console.log(this.originPoint.y - this.heights[0]);
            if(stpt.y - this.l[1] - 1 <= this.points[1].y - this.heights[1]) {   
                this.inAnimation[1] = false;
            }
            else {
                this.l[1] += 3;
            }
            this.path[9].rect(stpt.x + 1, stpt.y, this.widths[1] - 2, -this.l[1]);
            this.context.fillStyle = "skyblue";
            this.context.fill(this.path[9]);
        }

        // Fill pipe 1 - static
        fillPipe1() {
            // this.path[0].rect(this.originPoint.x, this.originPoint.y + 5, this.widths[0], -5);
            // this.context.fillStyle = "skyblue";
            // this.context.fill(this.path[0]);

            let fillPath = new Path2D();

            fillPath.rect(this.originPoint.x + 1, this.originPoint.y + 5, this.widths[0] - 2, -this.heights[0] - 6);
            this.context.fillStyle = "skyblue";
            this.context.fill(fillPath);

        }

        // Fill pipe number 7 - static
        fillPipe7() {
            this.context.fillStyle = "skyblue";
            this.context.fill(this.path[7]);
        }
    }

    // FluidContainer class inherits Lab class
    export class FluidContainer extends Lab {
     
        constructor(originPoint: Point, context: CanvasRenderingContext2D, canvas: HTMLCanvasElement) {
            super(canvas, context, originPoint);
        }

        //draw method overridden
        draw() {
            // this.path = new Path2D();

            //Circle on Path
            this.path.push(new Path2D());
            this.path[0].arc(this.originPoint.x, this.originPoint.y, 1.5, 0, 2*Math.PI);

            //Line Outside on Path
            this.path.push(new Path2D());
            this.path[1].moveTo(this.originPoint.x, this.originPoint.y);
            this.path[1].lineTo(this.originPoint.x + 50, this.originPoint.y);

            //Conatiner Box on Path
            this.path.push(new Path2D());
            this.path[2].rect(this.originPoint.x + 50, this.originPoint.y - 15, 30, 30);

            //Adding path to canvas
            for(let i = 0; i < this.path.length; i++) {
                this.context.strokeStyle = this.color;
                this.context.stroke(this.path[i]);
            }
        }

        showText(text: string) {

            // let textPath = new Path2D();
            this.clearText();
            this.context.font = "10px Arial";
            this.context.fillStyle = "black";
            this.context.fillText(text, this.originPoint.x + 50 + 5, this.originPoint.y - 15 + 15);
        
        }

        clearText() {
            this.context.clearRect(this.originPoint.x + 50 + 1, this.originPoint.y - 15 + 1, 28, 28);
        }

    }

    // Class LabButton for creating buttons on canvas
    export class LabButton {
        private originPoint: Point;
        private width: number;
        private height: number;
        private text: string = "";
        private path: Path2D;
        private context: CanvasRenderingContext2D;
        private canvas: HTMLCanvasElement;

        constructor(originPoint: Point, width: number, height: number, text: string, context: CanvasRenderingContext2D, canvas: HTMLCanvasElement) {
            this.originPoint = originPoint;
            this.width = width;
            this.height = height;
            this.text = text;
            this.context = context;
            this.canvas = canvas;
        }

        get origin_point() {
            return this.originPoint;
        }

        draw() {

            this.path = new Path2D();

            this.path.rect(this.originPoint.x, this.originPoint.y, this.width, this.height);
            
            this.context.font = "20px Arial";
            this.context.fillStyle = "black";
            this.context.fillText(this.text, this.originPoint.x + (this.width / 3.5), this.originPoint.y + (this.height / 1.25));
            this.context.lineWidth = 2;
            this.context.strokeStyle = "black";
            this.context.stroke(this.path);

        }

        // Check is point is inside a button
        isInside(pt: Point) {
            return this.context.isPointInPath(this.path, pt.x, pt.y);
        }

        // Hide the Lab Buttons
        hide() {
            this.context.clearRect(this.originPoint.x - 2, this.originPoint.y - 2, this.width + 3, this.height + 3);
        }

    }

}