// let canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("myCanvas");
// let context: CanvasRenderingContext2D = canvas.getContext("2d");
let output: HTMLInputElement = <HTMLInputElement>document.getElementById("result");
let lab: {lab: HeatTransferLab.Lab}[] = []; // Container to store all lab equipments

// let rect = canvas.getBoundingClientRect();
let userPoint: HeatTransferLab.Point; //Point clicked by user will be stored
let btn1: HeatTransferLab.LabButton;
let btn2: HeatTransferLab.LabButton;

let heights: number[] = []; // Height of each part of pipeline
let widths: number[] = [];  // Width of each part of pipeline

let pipeline: HeatTransferLab.Pipeline; //Pipleine class object
let hotFluidInlet: HeatTransferLab.FluidContainer; //Fluid container class object
let hotFluidOutlet: HeatTransferLab.FluidContainer; //Fluid container class object
let count: number = 0; // no of attempts

// drawLab(); //Draw the Lab

//Store all the drawn lab equipments in an array

// Function to check if point is inside a path and perform action accordingly
function mousehandle4(e: MouseEvent)
{
    // Variable to store the true/false state of user click on the LAB
    let ans0: boolean = false;
    
    //if exceeded max number of attempts, show the message
    // if(count > 2) {
    //     output.innerHTML = "<b>Exceeded maximum number of attempts.</b>";
    //     output.innerHTML += "<br>Your Score is 0</b>";
    //     output.style.color = "red";
    //     end_();
    // }
    
    let found: boolean = false; //Track if element is found or not
    //Point clicked by user
    userPoint = new HeatTransferLab.Point(e.clientX - rect.left, e.clientY - rect.top);
    // clearCanvas(); // Clear the canvas
    
    //Iterating the Lab equipements to find a point inside
    for(let i = 0; i < lab.length; i++) {
        lab[i].lab.color = "black";
        if(lab[i].lab.isInside(userPoint)) {
            //If the point is in Hot Fulid Inlet, Hot Fluid Inlet is at index 1
            if(i == 1) {
                lab[i].lab.color = "green";
                found = true;
                ans0 = true;
                counter_question++;
                output.innerHTML = "<b>Your score is " + (5 - count) + "</b>";
                output.style.color = "green";
                canvas.removeEventListener("click", mousehandle4, false);
                // end_(); //correct answer, end the quiz
            }
            else {
                lab[i].lab.color = "red";
            }
        } 
        lab[i].lab.draw();
    }
    if(ans0)
    {
        counter_question++;
    }
    
    // if incorrect attempt, show hints
    if(!found) {
        count++;
        document.getElementById("s4").style.display = "inline-block";
        ans0 = false;
    }
    console.log(ans0);
    
}

//Function to start the quiz
// function start_() {
    //     count = 0; //Initialize attempts to 0
    //     alert("Find out the Hot Fluid Inlet and click on it...");
//     canvas.addEventListener("click", mousehandle4, false);
//     output.innerHTML = "";
// }

//Function to stop the quiz
// function end_() {
    //     clearCanvas();
    //     drawLab();
    //     canvas.removeEventListener("click", mousehandle4, false); //remove click event
// }

// Function to clear the canvas
// function clearCanvas() {
    //     // context.clearRect(0, 0, canvas.width, canvas.height);
    // }
    
    //Function to draw the lab equipments
function drawPIPE() {
    console.log("pipe");
    //Heights of 8 different parts of pipeline
    heights.push(30, 160, 120, 8, 350, 8, 8, 8);
    
    //Widths of corresponding 8 parts of pipeline
    widths.push(8, 8, 8, 300, 8, 30, 30, 185);
    
    //Draw pipeline
    let originPoint = new HeatTransferLab.Point(200, 470);
    pipeline = new HeatTransferLab.Pipeline(originPoint, widths, heights, 270, context, canvas);
    pipeline.draw();
    lab.push({lab: pipeline});
    
    //Draw Hot Fluid Inlet
    let originPoint1 = new HeatTransferLab.Point(originPoint.x + 304, originPoint.y - 378);
    hotFluidInlet = new HeatTransferLab.FluidContainer(originPoint1, context, canvas);
    hotFluidInlet.draw();
    lab.push({lab: hotFluidInlet});
    
    //Draw Hot Fluid Outlet
    let originPoint2 = new HeatTransferLab.Point(originPoint.x + 304, originPoint.y - 135);
    hotFluidOutlet = new HeatTransferLab.FluidContainer(originPoint2, context, canvas);
    hotFluidOutlet.draw();
    lab.push({lab: hotFluidOutlet});

    let originPoint3 = new HeatTransferLab.Point(originPoint.x + 50, originPoint.y - 70);
    btn1 = new HeatTransferLab.LabButton(originPoint3, 30, 30, "<", context, canvas);
    // btn1.draw();

    let originPoint4 = new HeatTransferLab.Point(originPoint.x + 100, originPoint.y - 70);
    btn2 = new HeatTransferLab.LabButton(originPoint4, 30, 30, ">", context, canvas);
    btn2.draw();

}

// Function called on clicking next button
// function nextquestion() {

    // }