//declarations and usage of HTMLElements
// var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
// var context : CanvasRenderingContext2D = canvas.getContext("2d");
function rot_pump_buttons()
{
    container.draw(context);
    pump = new Pump.PumpContainer(new Geometries.Point(220,499),30,context);
    pump.draw();
    buttonContainer = new Geometries.Rectangle(new Geometries.Point(600,120),40,90,"white");
    buttonContainer.draw(context);
    button = new Buttons.Button(new Geometries.Circle(15,new Geometries.Point(625,140)),context,"red");
    button.draw();
    button.setText("P");
    button1 = new Buttons.Button(new Geometries.Circle(15,new Geometries.Point(665,140)),context,"red");
    button1.draw();
    button1.setText("H");   
}

var container : Rotameter.RotameterContainer = new Rotameter.RotameterContainer(new Geometries.Point(184,160),110,40,context);

var pump : Pump.PumpContainer;


var buttonContainer : Geometries.Rectangle ;

var button : Buttons.Button;
// button.draw();
// button.setText("P");

var button1 : Buttons.Button ;


var attempts = 0;
// let rect = canvas.getBoundingClientRect();
function mousehandle3(e: MouseEvent) {
    let p1 : Geometries.Point = new Geometries.Point(e.clientX-rect.left,e.clientY-rect.top);
    // this functions handles the question 3 with the mouse
    var ans0 = false;//make this variable true or false depending upon his selection. if required make this variable global
    //Button On Off Code on click
    if(button.isInside(p1)) {
        if(button.color == "red") {
            button.color = "green";
        }
        else if(button.color == "green") {
            button.color = "red";
        }
    }

    if(button1.isInside(p1)) {
        if(button1.color == "red") {
            button1.color = "green";
        }
        else if(button1.color == "green") {
            button1.color = "red";
        }
    }

    buttonContainer.draw(context);
    button.draw();
    button.setText("P");
    button1.draw();
    button1.setText("H");
    //Commented it for now.Use if necessary
    if(attempts > 1)
    {
        if(container.isInside(p1)) {
            container.changeColor("green");
            document.getElementById("res").style.display = "";   
            ans0 = true; 
            counter_question++;
            canvas.removeEventListener("click",mousehandle3,false);
        }
        else {
            // container.changeColor("red");
            attempts--;
            document.getElementById("s3").style.display = "";
            document.getElementById("res").innerHTML = "<br>Attempts Left = " + attempts;
            document.getElementById("res").style.display = "";
            //Code if clicked on the pump
            if(pump.isInside(p1)) {
                pump.changeColor("red");    
            }
        }
    } 
    else{
        //Wrong answer
        container.changeColor("green");
        document.getElementById("res").innerHTML = "Im sorry you were wrong";
        document.getElementById("res").style.display = "";
        document.getElementById("b3").style.display = "";
    }
    container.draw(context);
    pump.draw();
    //your code here
    //mouseclick event handler here which highlights red or green and make ans0 true or false
    // start_task_record(ans0);
    if (ans0) {
        //stop processing
        // counter_question++; //changing question counter
        //end_task(); this function will be provided for stoping time
        //coin will fly will take some time there the delay
        // setTimeout(nextquestion, 6000); 
    }
}

// function start_simulator() {
//     canvas.addEventListener("click",mousehandle3,false);
//     attempts = 3;
//     document.getElementById("q3").style.display = "";
// }

//mousehandle0 = Condensing Sleeve
//mousehandle1 = Cold water inlet
//mousehandle2 = Hot water tank
//mousehandle3 = Rotameter
//mousehandle4 = Hot Fluid Inlet