namespace Buttons {
    export class Button {
        private button : Geometries.Circle;
        private context : CanvasRenderingContext2D;
        private path : Path2D;
        private _color : string;

        constructor(button : Geometries.Circle,context : CanvasRenderingContext2D,color : string) {
            this.button = button;
            this.context = context;
            this._color = color;
        }

        draw() {
            this.path = new Path2D();
            this.button.drawPath(this.path);
            this.context.beginPath();
            this.context.fillStyle = "black";
            this.context.fill();
            this.context.strokeStyle = this._color;
            this.context.lineWidth = 2;
            this.context.stroke(this.path);
            this.context.closePath();
        }

        setText(str : string) {
            this.context.beginPath();
            this.context.font = "20px Georgia";
            this.context.fillText(str,this.button.cent.x-this.button.rad/2,this.button.cent.y+this.button.rad/2);
            this.context.fillStyle = this._color;
            this.context.fill();
            this.context.closePath();
        }

        isInside(pt : Geometries.Point) : boolean {
            console.log(pt);
            console.log(this.context.isPointInPath(this.path,pt.x,pt.y));
            if(this.context.isPointInPath(this.path,pt.x,pt.y)) {
                return true;
            }
            else {
                return false;
            }
        }

        set color(str :string) {
            this._color = str;
        }

        get color() : string {
            return this._color;
        }

    }
}

namespace Pump {
    export class PumpContainer {
        private srtPt : Geometries.Point;
        private length : number;
        private context : CanvasRenderingContext2D;
        private circle : Geometries.Circle;
        private triangle : Geometries.Triangle;
        private semiCircle : Geometries.Circle;
        private rect1 : Geometries.Rectangle;
        private rect2 : Geometries.Rectangle;
        private rect3 : Geometries.Rectangle;
        private path1 : Path2D;
        private path2 : Path2D;
        private color : string;
        private fillColor : string;

        constructor(srtPt : Geometries.Point,
                    length : number,
                    context : CanvasRenderingContext2D) {
                        this.srtPt = srtPt;
                        this.length = length;
                        this.context = context;
                        this.color = "black";
                        this.fillColor = "white";
        }

        draw() {
            this.path1 = new Path2D();
            this.path2 = new Path2D();
            this.triangle = new Geometries.Triangle(2*this.length,this.srtPt,60,false);
            this.triangle.drawPath(this.path1);
            this.circle = new Geometries.Circle(this.length,this.srtPt);
            this.circle.drawPath(this.path2);
            
            this.context.beginPath();
            this.context.fillStyle = "white";
            this.context.fill(this.path1);
            this.context.strokeStyle = this.color;
            this.context.lineWidth = 2;
            this.context.stroke(this.path1);
            this.context.closePath();

            this.context.beginPath();
            this.context.fillStyle = this.fillColor;
            this.context.fill(this.path2);
            this.context.strokeStyle = this.color;
            this.context.lineWidth = 2;
            this.context.stroke(this.path2);
            this.context.closePath();
        }

        isInside(pt : Geometries.Point) : boolean {
            if(this.context.isPointInPath(this.path1,pt.x,pt.y) || this.context.isPointInPath(this.path2,pt.x,pt.y)) {
                return true;
            }
            else {
                return false;
            }
        }
    
        changeColor(str : string) {
            this.color = str;
        }

        changeFillColor(str : string) {
            this.fillColor = str;
        }
    }
}

namespace Rotameter {
    //Class Container
    export class RotameterContainer {
        private srtPt : Geometries.Point;
        private height : number;
        private width : number;
        private path : Path2D;
        private context : CanvasRenderingContext2D;
        private color : string;
        private rectSrt : Geometries.Point;
        private triSrt : Geometries.Point;
        private widthinc : number;
        private heighinc : number;
        private srtPtForWater : Geometries.Point;
        private endPtForWater : Geometries.Point;
        private arrSrt : Geometries.Point[] = new Array();
        private arrEnd : Geometries.Point[] = new Array(); 
        private downHowMuch : number; 
        public inAnim : boolean;
 
        constructor(srtPt : Geometries.Point,
                    height : number,
                    width : number,
                    context : CanvasRenderingContext2D) {
            this.srtPt = srtPt;
            this.height = height;
            this.width = width;
            this.context = context;
            this.color = "black";
            this.rectSrt = new Geometries.Point(this.srtPt.x + (this.width/4),this.srtPt.y + this.height - 2*this.height/10);
            this.triSrt = new Geometries.Point(((this.srtPt.x + (this.width/4)) + (this.srtPt.x+this.width-(this.width/4)))/2,this.srtPt.y+this.height);
            this.widthinc = (this.width/(4*this.height));
            this.heighinc = 1;
            this.srtPtForWater =  new Geometries.Point(this.srtPt.x + (this.width/4)+2,this.srtPt.y + this.height-2);
            this.endPtForWater = new Geometries.Point(this.srtPt.x+this.width-(this.width/4)-2,this.srtPt.y+this.height-2);
            this.downHowMuch = this.height/6;
            // console.warn(this.downHowMuch);
            this.inAnim = false;
        }

        draw(context : CanvasRenderingContext2D) {
            this.path = new Path2D();
            this.path.moveTo(this.srtPt.x,this.srtPt.y);                                                    //drawing
            this.path.lineTo(this.srtPt.x+this.width,this.srtPt.y);                                         //the 
            this.path.lineTo(this.srtPt.x+this.width-(this.width/4),this.srtPt.y+this.height);              //outer
            this.path.lineTo(this.srtPt.x+(this.width/4),this.srtPt.y+this.height);                         //boundary
            this.path.lineTo(this.srtPt.x,this.srtPt.y);
            context.beginPath();
            context.fillStyle = "white";
            context.fill(this.path);
            context.strokeStyle = this.color;
            context.lineWidth = 2;
            context.stroke(this.path);
            context.closePath();

            this.drawWater();
            this.drawValve();

            context.beginPath();
            context.moveTo(this.srtPt.x,this.srtPt.y);                                                    //drawing
            context.lineTo(this.srtPt.x+this.width,this.srtPt.y);                                         //the 
            context.lineTo(this.srtPt.x+this.width-(this.width/4),this.srtPt.y+this.height);              //outer
            context.lineTo(this.srtPt.x+(this.width/4),this.srtPt.y+this.height);                         //boundary
            context.lineTo(this.srtPt.x,this.srtPt.y);
            context.strokeStyle = this.color;
            context.lineWidth = 2;
            context.stroke(this.path);
            context.closePath();

        }

        drawWater() {
            context.beginPath();
            context.moveTo(this.srtPt.x + (this.width/4),this.srtPt.y + this.height);
            for(let i=0;i<this.arrSrt.length;i++) {
                context.lineTo(this.arrSrt[i].x,this.arrSrt[i].y); 
            }
            if(this.arrEnd.length > 0)
            {
                context.lineTo(this.arrEnd[this.arrEnd.length-1].x,this.arrEnd[this.arrEnd.length-1].y);
            }
            context.moveTo(this.srtPt.x+this.width-(this.width/4),this.srtPt.y+this.height);
            for(let i=0;i<this.arrSrt.length;i++) {
                context.lineTo(this.arrEnd[i].x,this.arrEnd[i].y); 
            }
            context.lineTo(this.srtPt.x + (this.width/4),this.srtPt.y + this.height); 
            context.fillStyle = "skyblue";
            context.fill();
            context.strokeStyle = "skyblue";
            context.stroke();
            context.closePath();
        }
 
        drawValve() {
            //Drawing the inside float 
            let rect : Geometries.Rectangle = new Geometries.Rectangle(new Geometries.Point(this.rectSrt.x,this.rectSrt.y),2*(this.height/100),this.width/2);
            rect.draw(context);
            let triangle : Geometries.Triangle = new Geometries.Triangle(2*this.height/10,new Geometries.Point(this.triSrt.x,this.triSrt.y),80,true,"black");
            triangle.draw(context);
        }
 
        animate() {
            if(this.srtPt.y-2 < this.srtPtForWater.y) {
                if(this.srtPt.y < this.rectSrt.y) {
                    this.rectSrt.y--;
                    this.triSrt.y--;
                }
                this.draw(context);

                this.srtPtForWater.x -= this.widthinc;
                this.srtPtForWater.y -= this.heighinc;
                this.endPtForWater.x += this.widthinc;
                this.endPtForWater.y -= this.heighinc;

                this.arrSrt.push(this.srtPtForWater);
                this.arrEnd.push(this.endPtForWater);

            }
            else {
                this.inAnim = false;
            }
        }

        valveDown(flowRate : Number) : boolean {
            // console.log("flowRate = " + flowRate);
            if(flowRate == 660) {
                //nothing to do
            }
            else if(flowRate == 560) {
                var amt = this.srtPt.y+this.downHowMuch;
                // console.log(this.rectSrt.y + " " + amt);
                if(this.rectSrt.y < amt) {
                    this.rectSrt.y++;
                    this.triSrt.y++;
                    this.draw(context);
                    return true;
                }
                else{
                    return false;
                }
            }
            else if(flowRate == 460) {
                var amt = this.srtPt.y+2*this.downHowMuch;
                // console.log(this.rectSrt.y + " " + amt);
                if(this.rectSrt.y < amt) {
                    this.rectSrt.y++;
                    this.triSrt.y++;
                    this.draw(context);
                    return true;
                }
                else{
                    return false;
                }
            }
            else if(flowRate == 360) {
                var amt = this.srtPt.y+3*this.downHowMuch;
                // console.log(this.rectSrt.y + " " + amt);
                if(this.rectSrt.y < amt) {
                    this.rectSrt.y++;
                    this.triSrt.y++;
                    this.draw(context);
                    return true;
                }
                else{
                    return false;
                }
            }
            else if(flowRate == 260) {
                var amt = this.srtPt.y+4*this.downHowMuch;
                // console.log(this.rectSrt.y + " " + amt);
                if(this.rectSrt.y < amt) {
                    this.rectSrt.y++;
                    this.triSrt.y++;
                    this.draw(context);
                    return true;
                }
                else{
                    return false;
                }
            }
            else if(flowRate == 180) {
                var amt = this.srtPt.y+5*this.downHowMuch;
                // console.log(this.rectSrt.y + " " + amt);
                if(this.rectSrt.y < amt-6) {
                    this.rectSrt.y++;
                    this.triSrt.y++;
                    this.draw(context);
                    return true;
                }
                else{
                    return false;
                }
            }
        }

        isInside(pt : Geometries.Point) : boolean {
            if(this.context.isPointInPath(this.path,pt.x,pt.y)) {
                return true;
            }
            else {
                return false;
            }
        }

        changeColor(str : string) {
            this.color = str;
        }

        get _width() : number {
            return this.width;
        }

        get _height() : number {
            return this.height;
        }

    }
}

namespace Geometries{
    //Class Point
    export class Point {
        private _x : number;
        private _y : number;
        //constructor
        constructor(x : number,
                    y : number) {
                        this._x = x;
                        this._y = y;
        } 
        //getters for x and y
        get x() : number {
            return this._x;
        }
        get y() : number {
            return this._y;
        }
        //setters for x and y
        set x(x : number) {
            this._x = x;
        }
        set y(y : number) {
            this._y = y;
        }

        getLength(pt : Point) : number {
            let len : number = Math.sqrt(Math.pow((this._x-pt.x),2)+Math.pow((this._y-pt.y),2));
            console.log(len);
            return len;
        }
    }
    //class Rectangle
    export class Rectangle {
        private srtPt : Point;
        private height : number;
        private width : number;
        private color : string;
        constructor(srtPt : Point,
                    height : number,
                    width : number,
                    color : string = "black") {
                        this.srtPt = srtPt;
                        this.width = width;
                        this.height = height;
                        this.color = color;
        }
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.rect(this.srtPt.x,this.srtPt.y,this.width,this.height);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "black";
            context.stroke();
            context.closePath();
        }
    }
    //class Triangle
    export class Triangle {
        private length : number;
        private srtPt : Point;
        private angle : number;
        private goUp : boolean;
        private color : string;
        constructor(length : number,
                    srtPt : Point,
                    angle : number,
                    goUp : boolean,
                    color : string = "white") {
                        this.length = length;
                        this.srtPt = srtPt;
                        this.angle = angle;
                        this.goUp = goUp;
                        this.color = color;
        }
        draw(context : CanvasRenderingContext2D) {
            if(this.goUp) {
                this.angle = -this.angle;
            }
            context.beginPath();
            context.moveTo(this.srtPt.x,this.srtPt.y);
            context.lineTo(this.srtPt.x + this.length*Math.cos(this.angle*(Math.PI/180)),this.srtPt.y + this.length*Math.sin(this.angle*(Math.PI/180)));
            var insideAngle = 180-2*this.angle;
            context.lineTo(this.srtPt.x + this.length*Math.cos((this.angle+insideAngle)*(Math.PI/180)),this.srtPt.y + this.length*Math.sin((this.angle+insideAngle)*(Math.PI/180)));
            context.lineTo(this.srtPt.x,this.srtPt.y);
            context.fillStyle = this.color;
            context.fill();
            context.strokeStyle = "black";
            context.stroke();
            context.closePath();
        }
        drawPath(path : Path2D) {
            if(this.goUp) {
                this.angle = -this.angle;
            }
            path.moveTo(this.srtPt.x,this.srtPt.y);
            path.lineTo(this.srtPt.x + this.length*Math.cos(this.angle*(Math.PI/180)),this.srtPt.y + this.length*Math.sin(this.angle*(Math.PI/180)));
            var insideAngle = 180-2*this.angle;
            path.lineTo(this.srtPt.x + this.length*Math.cos((this.angle+insideAngle)*(Math.PI/180)),this.srtPt.y + this.length*Math.sin((this.angle+insideAngle)*(Math.PI/180)));
            path.lineTo(this.srtPt.x,this.srtPt.y);
        }
    }
    //class Circle
    export class Circle {
        private radius : number;
        private centre : Geometries.Point;
        private srtAngle : number;
        private endAngle : number;
        private direction : boolean;
        constructor(radius : number,
                    centre : Geometries.Point,
                    srtAngle : number = 0,
                    endAngle : number = 2*Math.PI,
                    direction : boolean = false) {
                        this.centre = centre;
                        this.radius = radius;
                        this.srtAngle = srtAngle;
                        this.endAngle = endAngle;
                        this.direction = direction;
        }
        draw(context : CanvasRenderingContext2D) {
            context.beginPath();
            context.arc(this.centre.x,this.centre.y,this.radius,this.srtAngle,this.endAngle,this.direction);
            context.fillStyle = "white";
            context.fill();
            context.strokeStyle = "black";
            context.stroke();
            context.lineWidth = 2;
            context.closePath();
        }
        drawPath(path : Path2D) {
            path.arc(this.centre.x,this.centre.y,this.radius,this.srtAngle,this.endAngle,this.direction);
        }
        get cent() : Point {
            return this.centre;
        }
        get rad() : number {
            return this.radius;
        }
    }

    export class Line {
        private srtPt : Point;
        private endPt : Point;
        private context : CanvasRenderingContext2D;
        constructor(srtPt : Point,endPt : Point,context : CanvasRenderingContext2D) {
            this.srtPt = srtPt;
            this.endPt = endPt;
            this.context = context; 
        }

        draw() {
            this.context.beginPath();
            this.context.moveTo(this.srtPt.x,this.srtPt.y); 
            this.context.lineTo(this.endPt.x,this.endPt.y);
            this.context.fillStyle = "skyblue";
            this.context.strokeStyle = "skyblue";
            this.context.lineWidth = 3;
            this.context.fill();
            this.context.stroke();
            this.context.closePath();
        }
    }
}
