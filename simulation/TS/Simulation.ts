function tank_fill(){
    //filling up the tank....
    var tank_water = new Hwtank.tank(context,416,422,100,178,10,"skyblue");
    tank_water.fill();
    a.drawHeater();
    // var pump = new Pump.PumpContainer(new Geometries.Point(220,499),30,context);
    // pump.draw();
    
}

function Start_Animation()
{
    //functions for animation of all parts must be called...
    tank_fill();
    pump.changeFillColor("skyblue");
    pump.draw();
    drawPIPE();

    pipeline.fillPipe7();
    pipeline.fillPipe1();

    canvas4.addEventListener('click', clickHandle, false);
}

// Function to fill after rotameter
function fillAfterRotameter() {
    if(pipeline.inAnimation[2]) {
        pipeline.fillPipe3();
        if(!pipeline.inAnimation[2]) {
            pipeline.inAnimation[3] = true;
        }
    }

    if(pipeline.inAnimation[3]) {
        pipeline.fillPipe4();
        if(!pipeline.inAnimation[3]) {
            pipeline.inAnimation[4] = true;
        }
    }

    if(pipeline.inAnimation[4]) {
        pipeline.fillPipe5();
        if(!pipeline.inAnimation[4]) {
            // pipeline.inAnimation[4] = true;
            return;
        }
        condensingSleeve.fillInnerTube = true;
    }
}

// Animation frames to fill pipe before rotameter
function fillpipe() {
    if(!pipeline.inAnimation[1]) {
        pipeline.inAnimation[2] = true;
        container.animate();
        if(!container.inAnim) {
            fillAfterRotameter();
        }
    }
    pipeline.fillPipe2();
    window.requestAnimationFrame(fillpipe);
}

function clickHandle(e: MouseEvent) {
    let rect = canvas4.getBoundingClientRect();
    if(button.isInside(new Geometries.Point(e.clientX - rect.left, e.clientY - rect.top))) {
        
        // console.log("Opened the valve...");
        // console.log("Filling pipe 2...");
        if(pipeline.valveColor === "black") {
            alert("Valve is not opened for water flow!");
            return;
        }
        button.color = "green";
        button.draw();

        pipeline.inAnimation[1] = true;
        fillpipe();

        // Rotameter Animation here (PENDING)
        // animValve();
        container.inAnim = true;
    }

    let clickedPoint = new CondensingSleeve.Point(e.clientX - rect.left, e.clientY - rect.top);
    condensingSleeve.toggleValve(clickedPoint);
    
    console.log(flowrate);
    if(btn2.isInside(new HeatTransferLab.Point(e.clientX - rect.left, e.clientY - rect.top))) {
            pipeline.drawValve("skyblue");
            btn2.hide();
            btn1.draw();
            flowrate = 660;
            showFlowRate();
    }

    if(btn1.isInside(new HeatTransferLab.Point(e.clientX - rect.left, e.clientY - rect.top))) {
        if(button1.color === "green") {
            addObservations();
            anim();
        }
    }

    if(button1.isInside(new Geometries.Point(e.clientX - rect.left, e.clientY - rect.top))) {
        
        // console.log("Heater");
        console.log(button.color);
        if(button.color != "green") {
            alert("Power ON first to start the heater!");
            return;
        }

        button1.color = "green";
        button1.draw();
        console.log("Heater ON");
        temperature.style.display = "inline-block";
        alert("Select the temperature...");
    }

    // If clicked on power button, fill the pipe upto rotameter

    // let clickedPoint = new CondensingSleeve.Point(e.x - rect.left, e.y - rect.top);
    // condensingSleeve.toggleValve(clickedPoint);
}

function anim() {
    if(container.valveDown(flowrate)){
        window.requestAnimationFrame(anim);
    }
}

