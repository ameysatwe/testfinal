//var div2:HTMLDivElement = <HTMLDivElement> document.getElementById("d2");
//displaying div 2...
//div2.style.display = "";


var ques1:HTMLSpanElement = <HTMLSpanElement> document.getElementById("ques1");
var ques2:HTMLSpanElement = <HTMLSpanElement> document.getElementById("ques2");
var ques3:HTMLSpanElement = <HTMLSpanElement> document.getElementById("ques3");

var op1:HTMLSpanElement = <HTMLSpanElement> document.getElementById("op1");
var op2:HTMLSpanElement = <HTMLSpanElement> document.getElementById("op2");
var op3:HTMLSpanElement = <HTMLSpanElement> document.getElementById("op3");

var result1:HTMLSpanElement = <HTMLSpanElement> document.getElementById("result1");
var result2:HTMLSpanElement = <HTMLSpanElement> document.getElementById("result2");
var result3:HTMLSpanElement = <HTMLSpanElement> document.getElementById("result3");



    
    //Displayiong Every question....
    ques1.style.display = "";
    ques1.innerHTML = "1. Which of the following is more correct for laminar flow?";

    ques2.style.display = "";
    ques2.innerHTML = "2. Which of the following is more correct for turbulent flow?";

    ques3.style.display = "";
    ques3.innerHTML = "3. Which of the following is more correct for Laminar flow?";

    //option values for Each question....
    var option_value1 = ["a. Re < 2100" , "b. 2100 < Re < 4000", "c. Re > 4000", "d. Re ≤ 2100"]; 
    var option_value2= ["a. Re > 2100", "b. Re > 4000", "c. 4000 < Re < 10000","d. Re > 10000"];
    var option_value3 = ["a. Hagen Poiseuille equation", "b. Stokes law", "c. Boundary layer", "d. Efflux time"];

    //Adding options
    generate_option(option_value1,op1,"pp");
    generate_option(option_value2,op2,"q");
    generate_option(option_value3,op3,"r");



//function to generate options....
function generate_option(option_value:string[],div_id,option_id:string)
{
    div_id.style.display = "";
    for(var i = 0;i<option_value.length;i++)
    {
        var choice :HTMLInputElement = <HTMLInputElement>document.createElement("input");
        choice.name = option_id
        choice.type =  "radio";
        choice.value = "answer";
        choice.id = option_id + i.toString();
        div_id.appendChild(choice);
        div_id.innerHTML += option_value[i] + " <br>"
    }

}
function test_result()
{
    //calculating for correct answer....
    var p11:HTMLInputElement =<HTMLInputElement> document.getElementById("pp0");
    var q2:HTMLInputElement =<HTMLInputElement> document.getElementById("q1")
    var r1:HTMLInputElement =<HTMLInputElement> document.getElementById("r0")
    if(p11.checked){
        result1.style.display = ""
        result1.style.color = "green"
        result1.innerHTML = "Right answer" 
    }else{
        result1.style.display = ""
        result1.style.color = "red"
        result1.innerHTML = "Wrong answer"
    }
    if(q2.checked){
        result2.style.display = ""
        result2.style.color = "green"
        result2.innerHTML = "Right answer" 
    }else{
        result2.style.display = ""
        result2.style.color = "red"
        result2.innerHTML = "Wrong answer"
    }
    if(r1.checked){
        result3.style.display = ""
        result3.style.color = "green"
        result3.innerHTML = "Right answer" 
    }else{
        result3.style.display = ""
        result3.style.color = "red"
        result3.innerHTML = "Wrong answer"
    }
}
