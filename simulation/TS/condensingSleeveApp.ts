// let canvas = <HTMLCanvasElement>document.getElementById("mycanvas");
// let context = canvas.getContext("2d");
let center = new CondensingSleeve.Point(500, 135);
let condensingSleeve = new CondensingSleeve.CondensingSleeveDiagram( center,
  context,
  170, //height of inner tube of condensing sleeve,
  8 // width of inner tube of condensing sleeve
); 
// let rect = canvas.getBoundingClientRect();
function sleeve()
{
  console.log("sleeve");
  center = new CondensingSleeve.Point(500, 135);
  condensingSleeve = new CondensingSleeve.CondensingSleeveDiagram(
    center,
    context,
    170, //height of inner tube of condensing sleeve,
    8 // width of inner tube of condensing sleeve
  );
  condensingSleeve.draw(); //draw the sleeve
}

function mousehandle0(e: MouseEvent) {
  // counter_question++;
  //handler for first question
  const pt = new CondensingSleeve.Point(e.clientX - rect.left, e.clientY - rect.top);
  let clickedPartName = condensingSleeve.isInside(pt); // possible values TUBE, INLET, OUTLET
  // context.clearRect(0, 0, canvas.width, canvas.height);
  var ans0 = clickedPartName === "TUBE"; //true if correct
  if (ans0) {
    // document.getElementById("b0").style.display = ""; //show next button
    counter_question++; // increment question
    condensingSleeve.highlightGreen(clickedPartName); // the clicked part with be highlighted in green
    canvas.removeEventListener("click", mousehandle0); //remove event listener
  } else {
    document.getElementById("s0").style.display = ""; // show hints
    condensingSleeve.highlightRed(clickedPartName); // clicked part should be highlighted red
  }
  condensingSleeve.draw(); //draw the sleeve
}

function mousehandle1(e: MouseEvent) {
  //handler for second question
  const pt = new CondensingSleeve.Point(e.clientX - rect.left, e.clientY - rect.top);
  let clickedPartName = condensingSleeve.isInside(pt); //get clicked part
  // context.clearRect(0, 0, canvas.width, canvas.height);
  var ans0 = clickedPartName === "INLET"; // true if INLET is clicked
  if (ans0) {
    //check for correct answer
    // document.getElementById("b1").style.display = ""; //show hints
    counter_question++;
    condensingSleeve.highlightGreen(clickedPartName);
    canvas.removeEventListener("click", mousehandle1);
  } else {
    document.getElementById("s1").style.display = "";
    condensingSleeve.highlightRed(clickedPartName);
  }
  condensingSleeve.draw();
}

//helper functions to make the demo program work
// function start_simulator() {
//   //start function
//   // condensingSleeve.draw(); //draw the diagram
//   document.getElementById("p0").style.display = ""; //show initial question
//   // canvas.addEventListener("click", mousehandle0); //add event handler
//   document.getElementById("sim1").style.display = "none"; // hide the start simulator button
// }

// function nextquestion() {
//   //will hide the answered question and unhide next question
//   document.getElementById("p" + (counter_question - 1)).style.display = "none"; //hide previous question
//   document.getElementById("p" + counter_question).style.display = ""; //display current question
//   condensingSleeve.highlightGreen(""); //reset the green drawing
//   // context.clearRect(0, 0, canvas.width, canvas.height); //clear the canvas
//   // condensingSleeve.draw(); //redraw the diagram without any green or red boundary
//   canvas.addEventListener("click", mousehandle1); // add next mouse handler
// }
