namespace CondensingSleeve {
  export class Point {
    x: number;
    y: number;
    constructor(x: number, y: number) {
      this.x = x;
      this.y = y;
    }
  }
  export class CondensingSleeveDiagram {
    name = "CondensingSleeve";
    private context: CanvasRenderingContext2D;
    public start: Point; //start point, please leave 70px in left and 120px in right and 55px in top for inlet and outlet
    private height: number; // height of inner tube
    private width: number;
    private pathInlet: Path2D = new Path2D(); // path of cold water inlet only (not thermometer)
    private pathTube: Path2D; //path of tube
    private pathOutlet: Path2D; //path of water outlet
    private pathValve: Path2D = new Path2D();
    private errorDraw = "";
    private successDraw = "";
    private shouldOuterFill = true;
    private outerFillProperties = { rightWidth: 0, leftWidth: 0, tubeHeight: 0 };
    private waterHeight = 0;
    private reverseInnerTubeFill = false;
    private reverseOuterTubeFill = true;
    private initial = true;

    // Added by Nirav
    private textCoords: number[][] = [];

    fillInnerTube = false;
    //possible values for error and success draw: INLET, OUTLET, TUBE,
    constructor(
      start: Point,
      context: CanvasRenderingContext2D,
      height = 300, //default
      width = 10 //default
    ) {
      this.start = start;
      this.context = context;
      this.height = height;
      this.width = width;
    }
    public highlightGreen(partName: string) {
      //partname will be outlined with green
      this.successDraw = partName;
      this.errorDraw = ""; // remove error outline
    }
    public highlightRed(partName: string) {
      //partname will be outlined with red
      this.successDraw = ""; //remove success outline
      this.errorDraw = partName;
    }
    public isInside(pt: Point): string {
      //returns name of part that was clicked
      if (this.context.isPointInPath(this.pathInlet, pt.x, pt.y) || this.context.isPointInPath(this.pathValve, pt.x, pt.y)) {
        return "INLET"; //inlet is clicked
      } else if (this.context.isPointInPath(this.pathOutlet, pt.x, pt.y)) {
        return "OUTLET"; //outlet is clicked
      } else if (this.context.isPointInPath(this.pathTube, pt.x, pt.y)) {
        return "TUBE"; //tube is clicked
      } else {
        return ""; // no click inside
      }
    }
    public toggleValve(pt: Point){
      console.log(pt.x, pt.y, this.context.isPointInPath(this.pathValve, pt.x, pt.y))
      if(this.context.isPointInPath(this.pathValve, pt.x, pt.y)){
        this.reverseOuterTubeFill = !this.reverseOuterTubeFill;
        this.outerFillProperties.leftWidth = 0;
        this.outerFillProperties.rightWidth = 0;
        this.outerFillProperties.tubeHeight = 0;
        this.outerFill();
        this.initial = false;

      }
    }
    private outerFill = () => {
      let shouldAnimate = false;
      if(this.reverseOuterTubeFill) {
        if (this.outerFillProperties.rightWidth < 84) {
          this.outerFillProperties.rightWidth += 4;
          shouldAnimate = true;
        }
         if (this.outerFillProperties.tubeHeight < (this.height - 20)) {
          this.outerFillProperties.tubeHeight += 4;
          shouldAnimate = true;
        }
        if (this.outerFillProperties.leftWidth < 52) {
          this.outerFillProperties.leftWidth += 4;
          shouldAnimate = true;
        }
      } else {
        if (this.outerFillProperties.rightWidth < 84) {
          this.outerFillProperties.rightWidth += 4;
          shouldAnimate = true;
        } else if (this.outerFillProperties.tubeHeight < (this.height - 20)) {
          this.outerFillProperties.tubeHeight += 4;
          shouldAnimate = true;
        }
        if (this.outerFillProperties.tubeHeight > (this.height - 45) && this.outerFillProperties.leftWidth < 52) {
          this.outerFillProperties.leftWidth += 4;
          shouldAnimate = true;
        }
      }
      
      if (shouldAnimate) {
        this.draw();
        requestAnimationFrame(this.outerFill);
      }
    }
    fillOuterTube() {
      this.reverseOuterTubeFill = false;
      this.outerFillProperties.leftWidth = 0;
      this.outerFillProperties.rightWidth = 0;
      this.outerFillProperties.tubeHeight = 0;
      this.outerFill();
    }
    emptyOuterTube() {
      this.reverseOuterTubeFill = true;
      this.outerFillProperties.leftWidth = 0;
      this.outerFillProperties.rightWidth = 0;
      this.outerFillProperties.tubeHeight = 0;
      this.outerFill();
    }
    draw() {
      this.drawOuterTube(); //draws the outer tube
      this.drawInnerTube(); //draws the inner tube
      this.drawLeftBox(); //draws outlet
      this.drawRightBox(); //draws cold water inlet
    }
    private drawLeftBox() {
      let startX = this.start.x - 20, //dont change this
        y1 = this.start.y + 30, //change for upper opening y
        y2 = y1 + 10; //change for lower opening y
      this.pathOutlet = new Path2D();
      this.pathOutlet.moveTo(startX, y1);
      this.pathOutlet.lineTo(startX - 30, y1);
      this.pathOutlet.lineTo(startX - 30, y1 - 20);
      this.pathOutlet.moveTo(startX - 40, y1 - 20);
      this.pathOutlet.lineTo(startX - 40, y1);
      this.pathOutlet.lineTo(startX - 50, y1);
      this.pathOutlet.moveTo(startX, y2);
      this.pathOutlet.lineTo(startX - 50, y2);
      this.context.save(); // save context
      this.context.strokeStyle = "black";
      if (this.successDraw === "OUTLET") {
        this.context.strokeStyle = "green";
      }
      if (this.errorDraw === "OUTLET") {
        this.context.strokeStyle = "red";
      }
      this.context.fillStyle = this.shouldOuterFill ? 'blue' : 'white'
      this.drawThermometer(startX - 35, (y1 + y2) / 2, this.pathOutlet);

      // Added by Nirav
      this.textCoords.push([startX - 35, (y1 + y2) / 2, 40, 40]);

      this.context.beginPath();
      if( !this.initial && this.reverseOuterTubeFill){
        this.context.clearRect(startX - 50, y1, 52, y2 - y1); //clear opening from outer tube
      this.context.fillRect(startX - 50, y1, 52 - this.outerFillProperties.leftWidth, y2 - y1)
      } else {
        this.context.fillRect(startX - 50, y1, 52, y2 - y1); //clear opening from outer tube
      this.context.clearRect(startX - 50, y1, 52 - this.outerFillProperties.leftWidth, y2 - y1)
      }
      this.context.stroke(this.pathOutlet);
      this.context.restore();
    }
    private drawRightBox() {
      let startX = this.start.x + 20 + this.width; //dont change this
      let y1 = this.start.y + this.height - 20; // change this to change lower opening y
      let y2 = y1 - 10; // change this to change upper opening y
      this.pathValve = new Path2D();
      this.pathValve.moveTo(startX + 80, y1);
      this.pathValve.lineTo(startX + 95, y2-2);
      this.pathValve.lineTo(startX + 95, y1+2);
      this.pathValve.lineTo(startX + 80, y2);
      this.pathValve.lineTo(startX + 80, y1);
      this.pathValve.closePath();
      this.pathInlet = new Path2D();
      this.pathInlet.moveTo(startX, y1); //draw inlet cold water
      this.pathInlet.lineTo(startX + 80, y1);
      this.pathInlet.lineTo(startX + 80, y2);
      this.pathInlet.lineTo(startX + 50, y2);
      this.pathInlet.lineTo(startX + 50, y2 - 30);
      this.pathInlet.moveTo(startX + 40, y2 - 30);
      this.pathInlet.lineTo(startX + 40, y2);
      this.pathInlet.lineTo(startX, y2);
      this.drawThermometer(startX + 45, (y1 + y2) / 2, this.pathInlet);
      
      // Added by Nirav
      this.textCoords.push([startX + 45, (y1 + y2) / 2, 40, 40]);

      this.pathInlet.closePath();
      this.context.save();
      this.context.beginPath();
      this.context.strokeStyle = 'black';
      if (this.successDraw === "INLET") {
        this.context.strokeStyle = "green";
      } else if (this.errorDraw === "INLET") {
        this.context.strokeStyle = "red";
      } 
      this.context.fillStyle = this.shouldOuterFill ? 'blue' : 'white';
      if( !this.initial && this.reverseOuterTubeFill){
        this.context.clearRect(startX - 2, y2 + 1, 84, y1 - y2); // clear pipe
      this.context.fillRect(startX - 2, y2 + 1, 84 - this.outerFillProperties.rightWidth, y1 - y2);
      } else {
        this.context.fillRect(startX - 2, y2 + 1, 84, y1 - y2); // clear pipe
      this.context.clearRect(startX - 2, y2 + 1, 84 - this.outerFillProperties.rightWidth, y1 - y2);
      }
      
      this.context.stroke(this.pathInlet);
      this.context.closePath();
      this.context.beginPath();
      this.context.save();
      if(!this.reverseOuterTubeFill){
        this.context.strokeStyle = 'blue';
      }
      this.context.stroke(this.pathValve);
      
      this.context.closePath();
      this.context.restore();
      this.context.restore();
    }

    // private animateInnerTube = () => {
    //   if (this.waterHeight <= this.height) {
    //     this.waterHeight++;
    //     this.drawInnerTube();
    //     requestAnimationFrame(this.animateInnerTube)
    //   }
    // }
    // public emptyInnerTube() {
    //   this.reverseInnerTubeFill = true;
    //   this.waterHeight = 0;
    //   this.animateInnerTube();
    // }
    // public fillInnerTube() {
    //   this.reverseInnerTubeFill = false;
    //   this.waterHeight = 0;
    //   this.animateInnerTube();
    // }
    private drawInnerTube() {
      this.context.save();
      this.context.strokeStyle = "black";
      this.context.beginPath();
      this.context.rect(this.start.x, this.start.y, this.width, this.height);
      this.context.fillStyle = this.fillInnerTube?  'skyblue': 'white';
      this.context.stroke();
      this.context.fillRect(this.start.x + 1,
        this.start.y - 1,
        this.width - 2,
        this.height - this.waterHeight + 2)
      
      

      this.context.restore();
    }
    private drawOuterTube() {
      this.pathTube = new Path2D();
      this.pathTube.rect(
        this.start.x - 20,
        this.start.y + 10,
        40 + this.width,
        this.height - 20
      );
      this.context.save();

      this.context.strokeStyle = "black";
      if (this.successDraw === "TUBE") {
        this.context.strokeStyle = "green";
      }
      if (this.errorDraw === "TUBE") {
        this.context.strokeStyle = "red";
      }
      this.context.beginPath();
      this.context.fillStyle = this.shouldOuterFill ? 'blue' : 'white'
      if (!this.initial && this.reverseOuterTubeFill) {
        this.context.fill(this.pathTube);
        this.context.clearRect(this.start.x - 20, this.start.y + 10, 40 + this.width, this.outerFillProperties.tubeHeight);
      } else {
        this.context.fill(this.pathTube);
        this.context.clearRect(this.start.x - 20, this.start.y + 10, 40 + this.width, this.height - 20 - this.outerFillProperties.tubeHeight);
      }
      this.context.stroke(this.pathTube);
      this.context.restore();
    }
    private drawThermometer(x: number, y: number, path: Path2D) {
      //tips of thermometer (circle) at x,y
      path.moveTo(x, y);
      path.arc(x, y, 2, 0, Math.PI * 2); //circle point
      path.moveTo(x, y);
      path.lineTo(x, y - 40);
      path.rect(x - 20, y - 80, 40, 40); //outward square
    }

    // Added by Nirav
    public showLeftText(text: string) {
      this.clearLeftText();
      this.context.fillStyle = "black";
      this.context.font = "10px Arial";
      this.context.fillText(text, this.textCoords[0][0] + 10 - 20, this.textCoords[0][1] - 60);
    }

    public clearLeftText() {
      this.context.clearRect(this.textCoords[0][0] + 1 - 20, this.textCoords[0][1] + 1 - 80, this.textCoords[0][2] - 2, this.textCoords[0][2] - 2);
    }

    // Added by Nirav
    public showRightText(text: string) {
      this.clearRightText();
      this.context.fillStyle = "black";
      this.context.font = "10px Arial";
      this.context.fillText(text, this.textCoords[1][0] + 10 - 20, this.textCoords[1][1] - 60);
    }

    public clearRightText() {
      this.context.clearRect(this.textCoords[1][0] + 1 - 20, this.textCoords[1][1] + 1 - 80, this.textCoords[1][2] - 2, this.textCoords[1][2] - 2);
    }
  }
}
