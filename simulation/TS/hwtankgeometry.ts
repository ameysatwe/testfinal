namespace Hwtank{

    export class tank 
    {
        name = "H W Tank";
        public path: Path2D;
        public path1: Path2D;
        public path3: Path2D;
        public context:CanvasRenderingContext2D;
        public color:string;
        public x:number;
        public y:number; 
        private part1:{x:number,y:number}[];
        public yes:boolean;
        
        public height:number;
        public length:number;
        public w:number;


        constructor(context:CanvasRenderingContext2D,x_coordinate_of_bottomLeftCorner:number,y_coordinate_of_bottomLeftCorner:number,height:number,length:number,width_of_pipe:number,color:string)
        {
            this.context = context;
            this.x = x_coordinate_of_bottomLeftCorner;
            this.y = y_coordinate_of_bottomLeftCorner;
            this.color = color;
            this.height = height;
            this.length = length;
            this.w = width_of_pipe;
        }

        drawTank()
        {
            this.path = new Path2D();
            //rectangle box starting from left corner in clockwise....
            this.path.moveTo(this.x,this.y);
            this.path.lineTo(this.x,this.y - 20);
            this.path.moveTo(this.x, this.y-20-this.w)
            this.path.lineTo(this.x, this.y - this.height);
            this.path.lineTo(this.x + 18, this.y - this.height);
            this.path.lineTo(this.x + 18,this.y - 20 - this.height);
            this.path.lineTo(this.x + 24,this.y - 20 - this.height);
            this.path.lineTo(this.x + 24,this.y  - this.height);
            this.path.lineTo(this.x + this.length/2 - this.w/2 ,this.y - this.height);
            this.path.moveTo(this.x + this.length/2 + this.w/2,this.y - this.height);
            this.path.lineTo(this.x + this.length - 24,this.y - this.height);
            this.path.lineTo(this.x + this.length - 24,this.y - this.height - 20);
            this.path.moveTo(this.x + this.length - 18,this.y - this.height - 20);
            this.path.lineTo(this.x + this.length - 18,this.y - this.height);
            this.path.lineTo(this.x + this.length,this.y - this.height);
            this.path.lineTo(this.x + this.length,this.y);
            this.path.lineTo(this.x + this.length/2 + 3 ,this.y);
            this.path.lineTo(this.x + this.length/2 + 3 ,this.y + 20);
            this.path.lineTo(this.x + this.length/2 - 3 ,this.y + 20);
            this.path.lineTo(this.x + this.length/2 - 3 ,this.y);
            this.path.lineTo(this.x ,this.y);
            //rectangle box completed....
            //sketching 2 close valve.... 
            this.path.moveTo(this.x + 18 ,this.y -this.height - 20);
            this.path.lineTo(this.x + 24 ,this.y -this.height - 30);
            this.path.lineTo(this.x + 18 ,this.y -this.height - 30);
            this.path.lineTo(this.x + 24 ,this.y -this.height - 20);
            this.path.moveTo(this.x + this.length/2 - 3 ,this.y + 20);
            this.path.lineTo(this.x + this.length/2  + 3  ,this.y + 30);
            this.path.lineTo(this.x + this.length/2 -3 ,this.y  + 30);
            this.path.lineTo(this.x +  this.length/2 + 3 ,this.y + 20);
            //last part down pipe.....
            this.path.moveTo(this.x + this.length/2 -3 ,this.y + 45);
            this.path.lineTo(this.x + this.length/2 -3 ,this.y + 30);
            this.path.moveTo(this.x + this.length/2 +3 ,this.y + 45);
            this.path.lineTo(this.x + this.length/2 +3 ,this.y + 30);
            
            this.context.strokeStyle = this.color;
            this.context.lineWidth = 2;
            this.context.stroke(this.path);

        }

        drawHeater()
        {
            this.path1 = new Path2D();    
            //heater beginning inner part......
            this.path1.moveTo(this.x + this.length + 21,this.y -this.height / 10 );
            this.path1.lineTo(this.x + 4*this.length/5,this.y -this.height / 10 );

            this.path1.arc(this.x + 4*this.length/5,this.y - this.height / 5, this.height/10 , Math.PI/2, 3*Math.PI/2, false);
            this.path1.lineTo(this.x + this.length + 21,this.y - 3*this.height/10);

            //heater back cylindrical part.....
            this.path1.moveTo(this.x + this.length +21,this.y - this.height/10);  
            this.path1.ellipse(this.x +this.length + 20,this.y - this.height / 5, this.height/10 + 5,10,Math.PI/2,0,2*Math.PI);
            this.path1.lineTo(this.x +this.length + 70 ,this.y - this.height / 10 + 5);

            //heater backend curved portion.....
            this.path1.ellipse(this.x +this.length + 70 ,this.y - this.height / 5,this.height/10 + 5,10,Math.PI/2,0,Math.PI,true) ; 
            this.path1.lineTo(this.x +this.length + 20 ,this.y - 3*this.height / 10 - 5);

            //writting heater.....
            this.context.font = "20pt cursive";
            this.context.fillText("Heater",this.x + this.length + 20,this.y + 15 ,500)
            this.context.fillText("T C",this.x + this.length + 82,this.y - this.height - 20 ,500)
            this.context.fillStyle  = "black"; 
            this.context.fill();            

            //temprature mesuring box connected to bob inside the tank...
            this.path1.moveTo(this.x + this.length - 17,this.y - 3*this.height/5);
            this.path1.arc(this.x + this.length - 21,this.y - 3*this.height/5,5,0,2*Math.PI);
            this.path1.arc(this.x + this.length - 21,this.y - 3*this.height/5,3,0,2*Math.PI);
            this.path1.arc(this.x + this.length - 21,this.y - 3*this.height/5,1,0,2*Math.PI);
            
            //display box w
            this.path1.moveTo(this.x + this.length - 21,this.y - 3*this.height/5);
            this.path1.lineTo(this.x + this.length - 21,this.y - this.height - 30); 
            this.path1.lineTo(this.x + this.length + 80,this.y - this.height - 30);
            this.path1.rect(this.x + this.length + 80,this.y - this.height - 55,50,50);
            this.path1.moveTo(this.x + this.length + 70,this.y - this.height - 5);

            //connecting pipe from box to heater back end......
            this.path1.moveTo(this.x + this.length + 105,this.y - this.height - 5);
            this.path1.lineTo(this.x + this.length + 105,this.y - this.height/5 - 5);
            this.path1.lineTo(this.x + this.length + 79,this.y - this.height/5 - 5);

            this.path1.moveTo(this.x + this.length + 110,this.y - this.height - 5);
            this.path1.lineTo(this.x + this.length + 110,this.y - this.height/5 + 5);
            this.path1.lineTo(this.x + this.length + 79,this.y - this.height/5 + 5);

            this.context.strokeStyle = this.color;
            this.context.lineWidth = 2;
            this.context.stroke(this.path1);

        }
        fill() 
        {
            this.path3 = new Path2D();
            this.path3.moveTo(this.x,this.y - this.height);
            this.path3.rect(this.x,this.y,this.length,this.height);
            this.context.fillStyle = this.color;
            this.context.fill(this.path3);
        }

        isinsideTank(x:number,y:number)
        {
            console.log(x,y);
            if(this.context.isPointInPath(this.path,x,y)){
                return  true;
            }else
            {
                return false
            }
        }
        isinsideHeater(x:number,y:number)
        {
            console.log(x,y);
            if(this.context.isPointInPath(this.path1,x,y)){
                return  true;
            }else
            {
                return false
            }
        }
    }

}