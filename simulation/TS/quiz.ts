var rndquestion: number[] = [];//stores random numbers
//if we display random questions there is a possibility that they might get repeated
var noofquestions: number = 5;
var counter_question = 0;
function random(min: number, max: number) {
    return (Math.round((max - min) * Math.random() + min));
    // var i = Math.round(Math.random()*rndquestion.length);
    // console.log(rndquestion[i]);
    // return rndquestion[i];
}
function start_sim() {
    draw_sketch();
    document.getElementById("sim1").style.display="none";
    document.getElementById("nextq").style.display="";
    //random  numbers storing in rndquestion
    while (rndquestion.length < noofquestions) {
        let rnd1 = random(0, noofquestions - 1);
        if (rndquestion.indexOf(rnd1) == -1) {
            rndquestion.push(rnd1);
            console.log()
        }
    }
    for(let i=0;i<rndquestion.length;i++)
    {
        console.log(rndquestion[i]);
    }
    //hiding all para pd questions
    hideallpara();
    //start_task() this function will be provided later for animating the coin and total time taken to complete the task
    next_question();
}

function hideallpara(){
    for (let i = 0; i < noofquestions; i++) {
        var para: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p" + i);
        para.style.display = "none";
    }
    para = <HTMLParagraphElement>document.getElementById("p" + rndquestion[counter_question]);
    para.style.display = "";
}

function next_question() {
    hideallpara();
    console.log(counter_question);
    if (counter_question < rndquestion.length) {
        switch (rndquestion[counter_question]) {
            case 0:
                canvas.addEventListener("click", mousehandle0); //condensing sleeve
                //call mouse handlinding function of question 0
                //canvas.addEventListner(mousehandle0)
                //mousehandle0(); //mouse click or move etc
                break;
            case 1:
                canvas.addEventListener("click", mousehandle1); // cold water inlet
                // end_();
                break;
            case 2:
                canvas.addEventListener("click",mousehandle2,false); //hot water tank
                //call mouse handlind function of question 0
                //mousehandle2(); 
                break;
            case 3:
                attempts=3;
                canvas.addEventListener("click",mousehandle3,false); //rotameter
                break;
            case 4:
                count=0;
                canvas.addEventListener("click", mousehandle4, false); // hot fluid inlet
                break;

        }
    }
    else {
        document.getElementById("d0").style.display="none";
        document.getElementById("d1").style.display="block";
        //do next actions as all questions are finished
    }
}

// function mousehandle(e: MouseEvent) {
//     // this functions handles the question 0 with the mouse
//     var ans0 = false;//make this variable true or false depending upon his selection. if required make this variable global

//     //your code here
//     //mouseclick event handler here which highlights red or green and make ans0 true or false
    
//         //start_task_record(ans0); this function will be proivided for calculating points
//     if (ans0) {
//         //stop processing
//         counter_question++; //changing question counter
//         //end_task(); this function will be provided for stoping time
//         //coin will fly will take some time there the delay
//         // setTimeout(nextquestion, 6000);
        
//     }
// }