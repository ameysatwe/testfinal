var table : HTMLTableElement = <HTMLTableElement>document.getElementById("tb1");
var table1 : HTMLTableElement = <HTMLTableElement>document.getElementById("tb2");
var d1 : number = 0.7;
var d2 : number = 1.0;
var D1 : number = 2.2;
var L : number = 100;
var S : number = 0;
var A : number = 0;

var area1 : number = 0;
var area2 : number = 0;

//getting input
var input:HTMLInputElement;

function getTable() {
    while(this.table.rows.length  > 1) {
        this.table.deleteRow(1);
    }
    table.style.display = "";
    document.getElementById("button1").style.display = "";
    makeRow("Inside Diameter of Inner Tube , d<sub>1</sub>(cm) : ","a1",table);
    makeRow("Outside Diameter of Inner Tube , d<sub>2</sub>(cm) : ","a2",table);
    makeRow("Inside Diameter of Outer Tube , D<sub>1</sub>(cm) : ","a3",table);
    makeRow("Length of Heat Exchanger , L(cm) : ","a4",table);
}

function makeRow(str : string,id : string,tab : HTMLTableElement) {
    var row = tab.insertRow();

    var cell : HTMLTableDataCellElement = row.insertCell();
    var txt : HTMLLabelElement = document.createElement("label");
    txt.style.textAlign = "center";
    txt.innerHTML = str;
    cell.appendChild(txt);

    var cell : HTMLTableDataCellElement = row.insertCell();             //insert cell in row
    var text : HTMLInputElement = document.createElement("input");          //creating element 
    text.type = "text";
    text.style.textAlign = "center";                        //aligning text to centre
    text.id = id;                                           //assigning id to the text boxes
    // console.log(text.id);
    cell.appendChild(text);
}

function verify() {
    let d1Input : number = getData("a1");
    let d2Input : number = getData("a2");
    let D1Input : number = getData("a3");
    let LInput : number = getData("a4");

    // console.log(d1Input + " " + d2Input + " " + D1Input + " " + LInput);
    if((d1Input-d1 == 0) && (d2Input-d2 == 0) && (D1Input-D1 == 0) && (LInput-L == 0)){
        area1 = Math.PI*d1*L;
        area2 = (Math.PI/4)*Math.pow(d1,2);
        console.log(area1);
        console.log(area2);

        input = <HTMLInputElement>document.getElementById("a1");
        input.style.backgroundColor = "green";

        input = <HTMLInputElement>document.getElementById("a2");
        input.style.backgroundColor = "green";

        input = <HTMLInputElement>document.getElementById("a3");
        input.style.backgroundColor = "green";

        input = <HTMLInputElement>document.getElementById("a4");
        input.style.backgroundColor = "green";

        while(this.table1.rows.length  > 1) {
            this.table1.deleteRow(1);
        }
        makeRow("Inside Heat transfer area of heat exchanger , A(cm<sup>2</sup>) : " , "a5",table1);
        makeRow("Cross-Sectional area of inner tube , S(cm<sup>2</sup>)","a6",table1);

        table1.style.display = "";
        document.getElementById("button2").style.display = "";
    }
    else {
        if(!(d1Input-d1 == 0)) {
            alert("Entered d1 value is wrong");
            input = <HTMLInputElement>document.getElementById("a1");
            input.style.backgroundColor = "red";
        }
        if(!(d2Input-d2 == 0)) {
            alert("Entered d2 value is wrong");
            input = <HTMLInputElement>document.getElementById("a2");
            input.style.backgroundColor = "red";
        } 
        if(!(D1Input-D1 == 0)) {
            alert("Entered D1 value is wrong");
            input = <HTMLInputElement>document.getElementById("a3");
            input.style.backgroundColor = "red";
        }
        if(!(LInput-L == 0)) {
            alert("Entered L value is wrong");
            input = <HTMLInputElement>document.getElementById("a4");
            input.style.backgroundColor = "red";
        }
        return;
    }
}

function getData(str : string) :number {
    input = <HTMLInputElement>document.getElementById(str);
    //parsing to number
    var num : number = +input.value;
    if(isNaN(num)) {                //validating the data
        alert("Enter Valid data");
        return 0;
    }
    else {
        return num;
    }
}

function verify1() {
    S = getData("a6");
    A = getData("a5");
    area1 = +area1.toFixed(1);
    console.warn(S - area2);
    console.warn(A - area1);
    if(Math.abs(S - area2) <= 0.11) {
        input = <HTMLInputElement>document.getElementById("a6");
        input.style.backgroundColor = "green";
        S = 219.8;
    }
    else {
        input = <HTMLInputElement>document.getElementById("a6");
        input.style.backgroundColor = "red";
    }

    if(Math.abs(A - area1) <= 0.21) {
        input = <HTMLInputElement>document.getElementById("a5");
        input.style.backgroundColor = "green";
        L = 0.3849;
    }
    else {
        input = <HTMLInputElement>document.getElementById("a5");
        input.style.backgroundColor = "red";
    }
}